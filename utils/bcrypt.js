const bcrypt = require('bcrypt');
const saltRounds = 10;

async function encryptPassword(password) {
  return await bcrypt.hash(password, saltRounds);
}

async function comparePassword(plainpassword, hash) {
  return await bcrypt.compare(plainpassword, hash);
}

module.exports = {
  encryptPassword,
  comparePassword
}