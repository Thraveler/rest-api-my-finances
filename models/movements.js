let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let MovementSchema = new Schema({
  description: {
    type: String,
    maxlength: 150
  },
  type: {
    type: String,
    required: true,
    enum: ['GASTO', 'INGRESO'],
    uppercase: true
  },
  amount: {
    type: Number,
    required: true
  },
  date_operation: {
    type: Date,
    default: Date.now()
  }
}, {
  timestamps: {}
});

module.exports = mongoose.model('Movement', MovementSchema);