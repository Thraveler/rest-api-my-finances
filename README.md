# Rest API My Finances

Rest API from the My Finaces project

***

## Usage

To use this project just download, install dependencies

```sh
$ npm install
```

Execute 

```sh
$ npm start
```

Check that it works by making a request to [http://localhost:3000](http://localhost:3000)

## Enjoy