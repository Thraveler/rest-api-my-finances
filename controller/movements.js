let MovementsService = require('../services/movements');

async function getMovements() {
  return await MovementsService.getMovements();
}

async function createMovement(movement) {
  return await MovementsService.createMovement(movement);
}

async function getMovement(movementId) {
  return await MovementsService.getMovement(movementId);
}

async function updateMovement(movementId, movementUpdated) {
  return await MovementsService.updateMovement(movementId, movementUpdated);
}

async function deleteMovement(movementId) {
  return await MovementsService.deleteMovement(movementId);
}

module.exports = {
  getMovements,
  createMovement,
  getMovement,
  updateMovement,
  deleteMovement
};