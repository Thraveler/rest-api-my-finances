let UserService = require('../services/users');
let MovementService = require('../services/movements');
let UtilBcrypt = require('../utils/bcrypt');

async function getUsers() {
  return await UserService.getUsers();
}

async function createUser(user) {
  try {
    let encryptedPass = await UtilBcrypt.encryptPassword(user.password);
    user.password = encryptedPass;

    return await UserService.createUser(user);
  } catch(error) {
    return error;
  }
}

async function getUser(userId) {
  return await UserService.getUser(userId);
}

async function getUserLogin(username, password) {
    let user = await UserService.getUserQuery(username);

    if(user) {
      let result = await UtilBcrypt.comparePassword(password, user.password);
      if(result)  return user;  
      throw new Error("Invalid password");
    }
    
    throw new Error("Invalid username");
}

async function updateUser(userId, userUpdated) {
  return await UserService.updateUser(userId, userUpdated);
}

async function addMovementUser(userId, movement) {
  let movementCreated = await MovementService.createMovement(movement);

  return await UserService.addUserMovement(userId, movementCreated._id);
}

async function deleteUser(userId) {
  return await UserService.deleteUser(userId);
}

module.exports = {
  getUsers,
  createUser,
  getUser,
  getUserLogin,
  updateUser,
  addMovementUser,
  deleteUser
};