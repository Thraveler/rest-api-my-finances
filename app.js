/* Import dependencies */
let express = require('express');
let logger = require('morgan');
let mongoose = require('mongoose');
let cors = require('cors');

require('dotenv').config();
let user_db = process.env.USER_DB;
let password_db = process.env.PASSWORD_DB;
let url_db = process.env.URL_DB;

let full_url_db = `mongodb://${user_db}:${password_db}@${url_db}?retryWrites=true&w=majority`

/* Import routes */
let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
let movementsRouter = require('./routes/movements');

/* Connect to the Database */
mongoose.connect(full_url_db,
  { 
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  }, 
  err => {
  if(err) {
    console.log("There was an error while connect to database");
    
    throw err
  };

  console.log('Database connection successfully');
});

/* Build app */
let app = express();

/* Use dependencies to the app */
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false}));
app.use(cors());

/* Add routes to the app */
app.use('/', indexRouter);
app.use('/api/users', usersRouter);
app.use('/api/movements', movementsRouter);

/* Response when request to non-existent endpoint */
app.use((req, res) => {
  res.json({
    message: 'Page not found'
  });
});

/* Response when there was an internal error */
app.use((err, req, res) => {
  res.json({
    error: err,
    message: 'There was an internal error'
  });
});

module.exports = app;