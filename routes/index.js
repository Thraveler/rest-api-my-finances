let express = require('express');
let router = express.Router();

/* GET Home page */
router.get('/', (req, res) => {
  res.json({
    message: "Hi, welcome to My Fiances API"
  });
});

module.exports = router;