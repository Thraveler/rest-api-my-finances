let express = require('express');
let router = express.Router();
let MovementController = require('../controller/movements')

/*
  * Route: /api/movements
*/
/* GET Retrieve all movements */
router.get('/', async (req, res) => {
  try {
    let movements = await MovementController.getMovements();
    
    // Check if something was retrieve else the movements are empty
    if(movements.length > 0) {
      res.status(200).json({ movements: movements });
    } else {
      res.status(404).json({ message: `Movements collection are empty` });
    }
  } catch (error) {
    res.status(500).json({
      error: error
    });
  }
});

/* POST Create a movement */
router.post('/', async (req, res) => {
  try {
    let movementCreated = await MovementController.createMovement(req.body);
    res.status(201).json({
      movement: movementCreated
    });
  } catch (error) {
    res.status(500).json({
      error: error
    });
  }
});

/*
  * Route: /api/movements/:movementId
*/
router.get('/:movementId', async (req, res) => {
  let movementId = req.params.movementId;

  try {
    let movement = await MovementController.getMovement(movementId);

    // Check if something was retrieve else the movement doesn't exist
    if(movement) {
      res.status(200).json({ movement: movement });
    } else {
      res.status(404).json({ message: `Movement with movementId ${movementId} not found` });
    }
  } catch(error) {
    res.status(500).json({
      error: error
    });
  }
});

router.patch('/:movementId', async (req, res) => {
  let movementId = req.params.movementId;
  let movementChanges = req.body;

  try {
    let movementUpdated = await MovementController.updateMovement(movementId, movementChanges);

    res.status(200).json({
      movement: movementUpdated
    });
  } catch(error) {
    res.status(500).json({
      error: error
    });
  }
});

router.delete('/:movementId', async (req, res) => {
  let movementId = req.params.movementId;

  try {
    let movementDeleted = await MovementController.deleteMovement(movementId);

    // Check if something was deleted else the movement doesn't exist
    if(movementDeleted) {
      res.status(200).json({ message: `Movement with movementId ${movementId} was deleted.` });
    } else {
      res.status(404).json({ message: `Movement with movementId ${movementId} not found` });
    }
  } catch(error) {
    res.status(500).json({
      error: error
    });
  }
});

module.exports = router;