let express = require('express');
let router = express.Router();
let UserController = require('../controller/users');

/*
  * Route: /api/users
*/
/* GET Retrieve all user */
router.get('/', async (req, res) => {
  try {
    let users = await UserController.getUsers();
    
    // Check if something was retrieve else the users are empty
    if(users.length > 0) {
      res.status(200).json({ users: users });
    } else {
      res.status(404).json({ message: `Users collection are empty` });
    }
  } catch (error) {
    res.status(500).json({
      error: error
    });
  }
});

/* POST Create an user */
router.post('/', async (req, res) => {
  try {
    let userCreated = await UserController.createUser(req.body);
    res.status(201).json({
      user: userCreated
    });
  } catch (error) {
    res.status(500).json({
      error: error
    });
  }
});

/*
  * Route: /api/users/:userId
*/
router.get('/:userId', async (req, res) => {
  let userId = req.params.userId;

  try {
    let user = await UserController.getUser(userId);

    // Check if something was retrieve else the user doesn't exist
    if(user) {
      res.status(200).json({ user: user });
    } else {
      res.status(404).json({ message: `User with userId ${userId} not found` });
    }
  } catch(error) {
    res.status(500).json({
      error: error
    });
  }
});

router.patch('/:userId', async (req, res) => {
  let userId = req.params.userId;
  let userChanges = req.body;

  try {
    let userUpdated = await UserController.updateUser(userId, userChanges);

    res.status(200).json({
      user: userUpdated
    });
  } catch(error) {
    res.status(500).json({
      error: error
    });
  }
});

router.delete('/:userId', async (req, res) => {
  let userId = req.params.userId;

  try {
    let userDeleted = await UserController.deleteUser(userId);

    // Check if something was deleted else the user doesn't exist
    if(userDeleted) {
      res.status(200).json({ message: `User with userId ${userId} was deleted.` });
    } else {
      res.status(404).json({ message: `User with userId ${userId} not found` });
    }
  } catch(error) {
    res.status(500).json({
      error: error
    });
  }
});

/*
  * Route: /api/users/movements/:userId
*/
router.post('/movements/:userId', async (req, res) => {
  let movement = req.body;
  let userId = req.params.userId;
  
  try {
    let userUpdated = await UserController.addMovementUser(userId, movement);

    res.status(201).json({
      user: userUpdated
    });
  } catch (error) {
    res.status(500).json({
      error: error
    });
  }
});

/*
  * Route: /api/users/login
*/
router.post('/login', async (req, res) => {
  let { username, password } = req.body;
  
  try {
    let userLogged = await UserController.getUserLogin({username}, password);

    res.status(201).json({
      user: userLogged
    });
  } catch (error) {
    res.status(500).json({
      error: error.message
    });
  }
});

module.exports = router;