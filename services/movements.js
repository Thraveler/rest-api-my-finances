let MovementModel = require('../models/movements');

async function getMovements() {
  return await MovementModel.find({}, 'description type amount date_operation');
}

async function createMovement(movement) {
  return await MovementModel.create(movement);
}

async function getMovement(movementId) {
  return await MovementModel.findById(movementId, 'description type amount date_operation');
}

async function updateMovement(movementId, movementUpdated) {
  return await MovementModel.findByIdAndUpdate(movementId, movementUpdated, { new: true, select: 'description type amount date_operation' });
}

async function deleteMovement(movementId) {
  return await MovementModel.findByIdAndDelete(movementId);
}

module.exports = {
  getMovements,
  createMovement,
  getMovement,
  updateMovement,
  deleteMovement
};
