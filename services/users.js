let UserModel = require('../models/users');

async function getUsers() {
  return await UserModel.find({}, 'username name lastname email');
}

async function createUser(user) {
  return await UserModel.create(user);
}

async function getUser(userId) {
  return await UserModel.findById(userId, 'username name lastname email movements').populate('movements');
}

async function getUserQuery(query) {
  return await UserModel.findOne(query, 'username name lastname email password');
}

async function updateUser(userId, userUpdated) {
  return await UserModel.findByIdAndUpdate(userId, userUpdated, { new: true, select: 'username name lastname email movements' });
}

async function addUserMovement(userId, movementId) {
  return await UserModel.findByIdAndUpdate(userId, { $push: { movements: movementId }}, { new: true, select: 'username name lastname email movements' });
}

async function deleteUser(userId) {
  return await UserModel.findByIdAndDelete(userId);
}

module.exports = {
  getUsers,
  createUser,
  getUser,
  getUserQuery,
  updateUser,
  addUserMovement,
  deleteUser
};
